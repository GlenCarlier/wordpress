<!doctype html>
<!-- [if lt IE 7]><html dir="ltr" lang="nl-be" class="no-js lt-ie7"><![endif]-->
<!-- [if lt IE 8]><html dir="ltr" lang="nl-be" class="no-js lt-ie8"><![endif]-->
<!-- [if lt IE 9]><html dir="ltr" lang="nl-be" class="no-js lt-ie9"><![endif]-->
<!-- [if lt IE 10]><html dir="ltr" lang="nl-be" class="no-js lt-ie10"><![endif]-->
<!-- [if (gt IE 9)|!(IE)]><!--><html dir="ltr" lang="nl-be" class="no-js" prefix="og: http://ogp.me/ns#"><!--<![endif]-->
<head>
    <!-- CHARACTER ENCODING -->
    <meta charset="UTF-8">
    <!-- LATEST VERSION OF RENDERING ENGINE -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <!-- MOST IMPORTANT SEO FRIENDLY TAG -->
    <title><?php wp_title(); ?></title>
    <!-- OTHER IMPORTANT SEO TAGS -->
    <meta name="description" content="Drdynscript wil graag met jullie zijn app en kennis delen omtrent HTML5, CSS3 en JavaScript.">
    <meta name="keywords" content="HTML5, CSS3, JavaScript, javaScript, jQuery, Canvas, Geeks">
    <meta name="author" content="Arteveldehogeschool | Bachelor in de Grafische en Digitale Media | DrDynscript">
    <meta name="copyright" content="Copyright 2003-14 Arteveldehogeschool. Alle rechten voorbehouden.">
    <!-- HUMANS AUTHORS TEXT FILE -->
    <link type="text/plain" rel="author" href="humans.txt">
    <!-- FAVICON AND TOUCHICONS ex: http://demosthenes.info/blog/467/Creating-MultiResolution-Favicons-For-Web-Pages-With-GIMP -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="content/icons/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="content/icons/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="content/icons/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="content/icons/apple-touch-icon-57-precomposed.png">
    <link href="content/icons/favicon.ico" rel="icon" type="image/x-icon">
    <link href="content/icons/favicon.ico" rel="shortcut icon" type="image/x-icon">
    <!-- MOBILE VIEWPORT -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- STYLE CSS REQUIRED -->
    <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
    <!-- BOOTSTRAP -->
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/components/bootstrap/css/bootstrap.min.css" type="text/css" media="screen" />
    <link href="http://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
    <!-- CSS -->
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/styles/app.css" type="text/css" media="screen" />
    <?php wp_head(); ?>
</head>
<body>
<!--[if lt IE 7]>
<p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.</p>
<![endif]-->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><?php bloginfo('name') ?></a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <div class="navbar">
                <div class="navbar-inner">
                    <div class="container">
                        <?php

                        $args = array(
                            'menu'              => 'menu',
                            'theme_location' => 'top-bar',
                            'depth'		 => 0,
                            'container'	 => false,
                            'container_class'   => 'collapse navbar-collapse',
                            'menu_class'	 => 'nav navbar-nav',
                            'walker'	 => new BootstrapNavMenuWalker()
                        );

                        wp_nav_menu($args);

                        ?>

                        <form class="navbar-form navbar-right" role="search">
                            <div class="btn-group">
                                <button type="register" class="btn btn-default"><?php echo wp_register('', ''); ?></button>
                                <button type="login" class="btn btn-primary"><?php wp_loginout($redirect); ?></button>
                            </div>
                            <div class="form-group">
                                <?php get_search_form(); ?>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>