<?php get_header(); ?>
    <div id="wrapper" class="rowWithFullWidth">
        <!-- Page content -->
        <div id="page-content-wrapper col-md-12">
            <div class="page-header">
                <h1><?php if (is_home()) { ?>
                        Home

                    <?php } else { ?>
                    <?php echo get_the_title($ID); ?>

                    <?php } ?></small></h1>
                <?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
            </div>

            <?php
            if (have_posts( )): while (have_posts()) : the_post();
                echo '<div class="panel panel-default">';
                the_title('<div class="panel-heading"><h3><?php the_title(); ?>','</h3></div>');
                echo '<div class="panel-body">';
                the_content('<p>','</p>');
                echo '</div>';
                echo '</div>';
            endwhile; else: ?>
                <div class="panel panel-default">
                    <p>Er zijn geen posts gevonden.</p>
                </div>
            <?php endif; ?>
        </div>
    </div>
    </div>
<?php get_footer(); ?>