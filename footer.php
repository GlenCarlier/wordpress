<div id="footer">
    <div class="container">
        <p>In opdracht van Arteveldehogeschool - Glen Carlier</p>
        <!-- Needs plugin: Floating Social Media icons -->
        <?php DISPLAY_ACURAX_ICONS(); ?>
    </div>
</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/components/bootstrap/js/bootstrap.min.js"></script>
<!-- JAVASCRIPT -->
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/scripts/app.js"></script>
</body>
</html>