<?php
/*
Template Name: Portfolio Page
*/
?>
<?php get_header(); ?>
    <div id="wrapper" class="rowWithFullWidth">
        <!-- Page content -->
        <div id="page-content-wrapper col-md-12">
            <div class="page-header">
                <h1><?php if (is_home()) { ?>
                        Home

                    <?php } else { ?>
                    <?php echo get_the_title($ID); ?>

                    <?php } ?></small></h1>
                <?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
            </div>
            <div class="container">
            <div class="row">
            <?php

            query_posts( 'post_type=portfolio');
            if (have_posts( )): while (have_posts()) : the_post();
                echo'<div class="col-sm-3">';
                echo '<div class="panel panel-default panelPortfolio">';
                echo '<div class="panel-heading">';
                /* Als de titel langer is dan...*/
                echo '<h3 class="portfolioTitle">';
                if (strlen($post->post_title) > 12) {
                    echo substr(the_title($before = '', $after = '', FALSE), 0, 12) . '...'; } else {
                    the_title();
                }
                echo '</h3>';

                $terms = get_the_terms( $post->ID , 'portfolio_category' );

                foreach ( $terms as $term ) {
                    echo '<p> ';
                    echo $term->name;
                    echo '</p>';

                }
                echo '</div>';
                echo '<div class="panel-body thumbBody">';
                the_post_thumbnail('thumbnail', array('class' => 'img-responsive thumbimg'));
                the_category();
                echo '</div>';
                echo '</div>';
                echo '</div>';



                //wp_list_categories( 'taxonomy=portfolio_category' );
            endwhile; else: ?>
                <div class="panel panel-default">
                    <p>Er zijn geen posts gevonden.</p>
                </div>
            <?php endif; ?>
            </div>
            </div>
        </div>
    </div>
    </div>
<?php get_footer(); ?>