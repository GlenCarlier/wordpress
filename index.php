<?php get_header(); ?>
    <div id="wrapper" class="rowWithFullWidth">
        <!-- Page content -->
        <div id="page-content-wrapper col-md-12">
            <div class="page-header">
                <h1><?php if (is_home()) { ?>
                        Home

                    <?php } else { ?>
                    <?php echo get_the_title($ID); ?>

                    <?php } ?></small></h1>
                <?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
            </div>
            <!-- Keep all page content within the page-content inset div! -->
            <div class="page-content inset">
                <?php if (have_posts( )): while (have_posts()) : the_post();
                    echo '<div class="panel panel-default">';
                    the_title('<div class="panel-heading"><h3><?php the_title(); ?>','</h3></div>');
                    echo '<div class="panel-body">';
                    the_content('<p>','</p>');
                    the_category();
                    $args = array(
                        'number' => '5',
                        'post_id' => get_the_ID()
                    );
                if (!is_category() && !is_archive()) {
                // add subpage list if single page
                $children = wp_list_pages('title_li=&echo=0&child_of=' . $post->ID);
                if ($children) {
                echo '<div class="mychildren">';
                    echo '<h2>Inside This Section:</h2>';
                    echo '<ul>';
                        echo $children;
                     echo '</ul>';
                     }
                    }
                    echo '</div>';

                    echo '</div>';
                endwhile; else: ?>
                    <div class="panel panel-default">
                        <p>Er zijn geen posts gevonden.</p>
                    </div>
                <?php endif; ?>


            </div>
        </div>
    </div>
<?php get_footer(); ?>