<!-- Custom JavaScript for the Menu Toggle -->
$("#menu-toggle").click(function(e) {
    e.preventDefault();
    $("#wrapper").toggleClass("active");
});



/*
SIDEBAR NIET MEER IN GEBRUIK

$(".toggledown").hide();
$(".sidebar-nav").on("click", "li", function(){
    $(this).children(".toggledown").toggle();
    $(this).find('.pijltje').toggleClass("glyphicon glyphicon-chevron-right");
    $(this).find('.pijltje').toggleClass("glyphicon glyphicon-chevron-down");
});
*/

/* ZORGDE VOOR OVERAL KLIKBARE NAVIGATIE -> Obsolete

$('ul.nav li.dropdown, ul.nav li.dropdown-submenu').hover(function() {
    $(this).find(' > .dropdown-menu').stop(true, true).delay(0).fadeIn();
}, function() {
    $(this).find(' > .dropdown-menu').stop(true, true).delay(0).fadeOut();
});

$('a.dropdown-toggle').click(function(e) {
    window.location.replace($(this).attr("href"));
});

*/


//COMMENTS
$(".comment_switch").click(function () {
    //$(".comments").toggleClass("hidden");
    $(this).closest('div').find('.comments').toggleClass("hidden");
});

$(".showComments_switch").click(function () {
    //$(".comments").toggleClass("hidden");
    $(this).closest('div').find('.showComments').toggleClass("hidden");
});

//FILTER
$(".postFilter").hide();
$(".categoryFilter").hide();
$(".commentFilter").hide();
$(".userFilter").hide();

$(".btnPost").on("click",function(){
    $(".postFilter").toggle();
    $(".categoryFilter").hide();
    $(".commentFilter").hide();
    $(".userFilter").hide();
});

$(".btnCategory").on("click",function(){
    $(".postFilter").hide();
    $(".categoryFilter").toggle();
    $(".commentFilter").hide();
    $(".userFilter").hide();
});

$(".btnComment").on("click",function(){
    $(".postFilter").hide();
    $(".categoryFilter").hide();
    $(".commentFilter").toggle();
    $(".userFilter").hide();
});

$(".btnUser").on("click",function(){
    $(".postFilter").hide();
    $(".categoryFilter").hide();
    $(".commentFilter").hide();
    $(".userFilter").toggle();
});

$( window ).load(function() {

    boxes = $('.col-sm-6');
    maxHeight = Math.max.apply(
        Math, boxes.map(function() {
            return $(this).height();
        }).get());
    boxes.height(maxHeight);
    $('.col-sm-12 .panel').height(maxHeight/2-22);//22 = 20 (bottom-margin) + 2 *1 (border)
});

