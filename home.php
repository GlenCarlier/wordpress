<?php get_header(); ?>
    <div id="wrapper" class="rowWithFullWidth">
        <!-- Page content -->
        <div id="page-content-wrapper col-md-12">
            <div class="page-header">
                <h1><?php if (is_home()) { ?>
                        Home

                    <?php } else { ?>
                    <?php echo get_the_title($ID); ?>

                    <?php } ?></small></h1>
                <?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
            </div>
            <!-- Keep all page content within the page-content inset div! -->
            <div class="page-content inset">
                <div class="alert alert-success">
                    <a href="#" class="alert-link">All the posts are displayed below in a chronologic order.</a>
                </div>
                <!-- List of the buttons with linked panels-->
                <div class="btn-group btn-group-justified">
                    <div class="btn-group">
                        <button type="button" class="btn btn-default btnPost"><span class="label label-primary"><?php $count_posts = wp_count_posts(); echo $count_posts->publish; ?></span> Posts <span class="glyphicon glyphicon-pushpin"></span></a></button>
                    </div>
                    <?php
                    $category_count = $wpdb->get_var("SELECT COUNT(*) FROM $wpdb->term_taxonomy WHERE taxonomy = 'category'")
                    ;?>
                    <div class="btn-group">
                        <button type="button" class="btn btn-default btnCategory"><span class="label label-success"><?php echo $category_count; ?></span> Pages <span class="glyphicon glyphicon-book"></span></a></button>
                    </div>
                    <?php
                    $numcomms = $wpdb->get_var("SELECT COUNT(*) FROM $wpdb->comments WHERE comment_approved = '1'");
                    if (0 < $numcomms) $numcomms = number_format($numcomms);
                    ?>
                    <div class="btn-group">
                        <button type="button" class="btn btn-default btnComment"><span class="label label-warning"><?php echo $numcomms;?></span> Comments <span class="glyphicon glyphicon-tags"></span></a></button>
                    </div>
                    <div class="btn-group">
                        <button type="button" class="btn btn-default btnUser"><span class="label label-danger"><?php $result = count_users(); echo $result['total_users']; ?></span> Users <span class="glyphicon glyphicon-user"></span></a></button>
                    </div>
                </div>
                <!-- List of the panels linked to buttons above -->
                <div class="panel panel-default">
                    <div class="panel-body postFilter">
                        <p><b>List of all Posts:</b></p>
                        <?php
                        query_posts('showposts=10');
                        if ( have_posts() ){
                            echo '<ul>';
                            while ( have_posts() ){
                                the_post(); ?>

                                <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                            <?php
                            }
                            echo '</ul>';
                        }
                        ?>
                    </div>
                    <div class="panel-body categoryFilter">
                        <p><b>List of all other Pages:</b></p>
                        <?php
                        echo '<ol>';
                        wp_list_pages( array( 'title_li' => null ) );
                        echo '</ol>';
                        ?>
                    </div>
                    <div class="panel-body commentFilter">
                        <p><b>List of recent Comments:</b></p>
                        <?php
                        global $wpdb;
                        $sql = "SELECT DISTINCT ID, post_title, post_password, comment_ID, comment_post_ID, comment_author, comment_date_gmt, comment_approved, comment_type,comment_author_url, SUBSTRING(comment_content,1,30) AS com_excerpt FROM $wpdb->comments LEFT OUTER JOIN $wpdb->posts ON ($wpdb->comments.comment_post_ID = $wpdb->posts.ID) WHERE comment_approved = '1' AND comment_type = '' AND post_password = '' ORDER BY comment_date_gmt DESC LIMIT 10";

                        $comments = $wpdb->get_results($sql);
                        $output = $pre_HTML;
                        $output .= "\n<ul>";
                        foreach ($comments as $comment) {
                            $output .= "\n<li>".strip_tags($comment->comment_author) .":" . "<a href=\"" . get_permalink($comment->ID)."#comment-" . $comment->comment_ID . "\" title=\"on ".$comment->post_title . "\">" . strip_tags($comment->com_excerpt)."</a></li>";
                        }
                        $output .= "\n</ul>";
                        $output .= $post_HTML;
                        echo $output;
                        ?>
                    </div>
                    <div class="panel-body userFilter">
                        <p><b>All registered users:</b></p>
                        <?php
                        $order = 'user_nicename';
                        $users = $wpdb->get_results("SELECT * FROM $wpdb->users ORDER BY $order"); // query users
                        foreach($users as $user) : // start users' profile "loop"
                            ?>
                            <?php echo $user->user_login; ?>
                            <?php echo $user->user_firstname; ?>
                            <?php echo $user->user_lastname; ?>
                            <?php // echo $user->user_nickname; ?>
                            <?php // echo $user->user_email; ?>

                            <?php //echo $user->user_email; ?>
                            <?php // echo $user->user_url; ?>
                            <?php echo $user->user_icq; ?>
                            <?php echo $user->user_aim; ?>
                            <?php echo $user->user_msn; ?>
                            <?php echo $user->user_yim; ?>
                            <?php // echo $user->user_nicename; ?>
                            <?php echo "<br />"; ?>
                        <?php
                        endforeach; // end of the users' profile 'loop'
                        ?>
                    </div>
                </div>
                <!-- Get all posts in Chronologic order -->
                <?php if (have_posts( )): while (have_posts()) : the_post();
                    echo '<div class="panel panel-default">';
                    the_title('<div class="panel-heading"><h3><?php the_title(); ?>','</h3></div>');
                    echo '<div class="panel-body">';
                    echo '<p>';
                    the_time('jS F Y');
                    echo ' by ';
                    the_author_nickname();
                    echo '</p>';
                    the_content('<p>','</p>');

                    //COMMENTS
                    $args = array(
                        'post_id' => get_the_ID()
                    );

                    //Comment Buttons
                    echo '<button class="btn btn-default"><a class="showComments_switch">';
                    //'No Comments »', '1 Comment »', '% Comments »'
                    comments_number('No Comments »', '1 Comment »', '% Comments »');
                    echo '</a></button>';
                    echo '<button class="btn btn-default"><a class="comment_switch">Add Comment</a></button>';
                    //Latest Comments
                    echo '<div class="showComments hidden"> ';

                    if (get_comments_number()==0) {
                        echo'<h3>No comments yet.</h3>';
                    } else {
                        echo'<h3>Latest Comments</h3>';
                    }
                    foreach (get_comments($args) as $comment){
                        echo '<div class="info-block block-info clearfix">';
                        //echo '<div class="square-box pull-left"><span class="glyphicon glyphicon-user glyphicon-lg"></span></div>';
                        echo '<div class="square-box pull-left">';
                        echo get_avatar( $comment, 100 );
                        echo '</div>';

                        //GET AVATER DOESN'T WORK
                        echo $comment->get_avatar;
                        echo'<h5>';
                        echo $comment->comment_date;
                        echo '</h5>';
                        echo'<h4>';
                        echo $comment->comment_author;
                        echo '</h4>';
                        echo '<span>';
                        echo $comment->comment_content;
                        echo '</span>';
                        echo '</div>';
                    }
                    echo '</div>';
                    //Add Comment Form
                    echo '<div class="comments hidden"> ';
                    comment_form();
                    echo '</div>';
                    echo '</div>';

                    echo '</div>';
                endwhile; else: ?>
                    <div class="panel panel-default">
                        <p>Er zijn geen posts gevonden.</p>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
<?php get_footer(); ?>